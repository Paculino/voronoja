#!/usr/bin/python
# -*- coding: utf-8 -*-

# 31 seconds on fifth scale euclidean; 487-550 fifth scale ellipsoidal (427 with itertools and optimization3);
# 167 fifth scale ellipsoidal with itertools+optimization3; 3735 to 4480 full size with itertools+optimization3; ~2000 full size with extended borders, itertools, and optimization5

from PIL import Image, ImageDraw, ImageFilter, ImageOps
import math
import random
import time
import numpy as np
import itertools
import colorsys
Ek = time.time()

np.seterr('raise')

# Thank you, Thaddeus Vincenty
# https://geographiclib.sourceforge.io/geodesic-papers/vincenty75b.pdf

# -125 degrees is westernmost, -66 degrees is easternmost
## 24 is southernmost, 50 is northernmost
# seeds are from the top 54 largest MSA in the USA, excluding the MSAs without a city in the top 100 largests cities in the USA, and choosing for the representative city for each MSA the most densely populated city in the name of the MSA; messing with Texas was allowed

##SeedLats = np.array([40.712778, 34.05, 41.881944, 32.705, 29.762778, 38.9101, 25.775163, 39.952778, 33.755, 33.3, 42.373611, 37.7775, 33.948056, 42.331389, 47.609722, 44.981944, 32.627778, 27.973611, 39.7392, 38.627222, 39.289444, 35.227222, 28.303889, 29.425, 45.52, 38.581667, 40.439722, 36.081944, 30.508611, 39.1, 39.099722, 39.962222, 39.768611, 41.483333, 37.354444, 35.846111, 36.916667, 43.05, 30.336944, 35.468611, 35.766667, 35.1175, 37.533333, 29.997778, 38.256111, 42.904722, 32.221667, 36.75])
##SeedLons = np.array([-74.006111, -118.25, -87.627778, -97.122778, -95.383056, -77.0147, -80.208615, -75.163611, -84.39, -111.833333, -71.110556, -122.416389, -117.396111, -83.045833, -122.333056, -93.269167, -117.048056, -82.764167, -104.985, -90.197778, -76.615278, -80.843056, -81.412778, -98.493889, -122.681944, -121.494444, -79.976389, -115.124722, -97.678889, -84.516667, -94.578333, -83.000556, -86.158056, -81.666667, -121.969167, -86.391944, -76.2, -87.95, -81.661389, -97.521389, -78.633333, -89.971111, -77.466667, -90.1775, -85.751389, -78.849444, -110.926389, -119.766667])

SeedLats = np.array([
    40.712778,
    33.768333,
    41.881944,
    32.705,
    29.762778,
    38.9101,
    39.952778,
    25.775163,
    33.755,
    42.373611,
    33.3,
    37.7775,
    33.948056,
    42.331389,
    47.609722,
    44.981944,
    32.627778,
    27.973611,
    39.7392,
    39.289444,
    38.627222,
    28.303889,
    35.227222,
    29.425,
    45.52,
    38.581667,
    40.439722,
    30.508611,
    36.175,
    39.1,
    39.099722,
    39.962222,
    39.768611,
    41.483333,
    37.371111,
    35.846111,
    36.916667,
    41.823611,
    30.336944,
    43.05,
    35.468611,
    35.766667,
    35.1175,
    37.533333,
    38.256111,
    29.997778,
    40.760833,
    41.7625,
])
SeedLons = np.array([
    -74.006111,
    -118.195556,
    -87.627778,
    -97.122778,
    -95.383056,
    -77.0147,
    -75.163611,
    -80.208615,
    -84.39,
    -71.110556,
    -111.833333,
    -122.416389,
    -117.396111,
    -83.045833,
    -122.333056,
    -93.269167,
    -117.048056,
    -82.764167,
    -104.985,
    -76.615278,
    -90.197778,
    -81.412778,
    -80.843056,
    -98.493889,
    -122.681944,
    -121.494444,
    -79.976389,
    -97.678889,
    -115.136389,
    -84.516667,
    -94.578333,
    -83.000556,
    -86.158056,
    -81.666667,
    -122.0375,
    -86.391944,
    -76.2,
    -71.422222,
    -81.661389,
    -87.95,
    -97.521389,
    -78.633333,
    -89.971111,
    -77.466667,
    -85.751389,
    -90.1775,
    -111.891111,
    -72.674167,
])

SemimajorAxis = 6378137.0  # a
flattening = 1 / 298.257223563  # f
SemiminorAxis = (1 - flattening) * SemimajorAxis  # b


def RealDistance(
    StartLat,
    StartLon,
    EndLat,
    EndLon,
):
    L = np.radians(EndLon - StartLon)
    Ua = np.arctan((1 - flattening) *
                   np.tan(np.radians(StartLat)))  # U1
    # U2                 reduced latitude (latitude on the auxiliary sphere)
    Ub = np.arctan((1 - flattening) * np.tan(np.radians(EndLat)))

    cosUa = np.cos(Ua)
    cosUb = np.cos(Ub)
    sinUa = np.sin(Ua)
    sinUb = np.sin(Ub)

    Convergence = 100
    lamdba = L

    while np.any(Convergence > 10):
        cosLamdba = np.cos(lamdba)
        sinLamdba = np.sin(lamdba)
        sinsigma = np.sqrt((cosUb * sinLamdba) ** 2 + (cosUa * sinUb
                           - cosUb * sinUa * cosLamdba) ** 2)
        cossigma = sinUa * sinUb + cosUa * cosUb * cosLamdba
        sigma = np.arctan2(sinsigma, cossigma)
        try:
            sinAlpha = cosUa * cosUb * sinLamdba / sinsigma
            cosTwoSigmaMid = cossigma - 2 * sinUa * sinUb / (1
                                                             - sinAlpha ** 2)
            Cbar = flattening * (1 - sinAlpha ** 2) * (4 + flattening
                                                       * (4 - 3 * (1 - sinAlpha ** 2))) / 16
            lamdba = L + (1 - Cbar) * flattening * sinAlpha * (sigma
                                                               + Cbar * sinsigma * (cosTwoSigmaMid + Cbar
                                                                                    * cossigma * (2 * cosTwoSigmaMid ** 2 - 1)))
            if np.any(np.abs(lamdba) > np.pi) and np.any(np.abs(180
                                                                - np.hypot(StartLat - EndLat, StartLon - EndLon))
                                                         < 5):
                print(
                    'Seed points and/or mapping area include (nearly) antipodal points')
                break
            uSquared = (1 - sinAlpha ** 2) * (SemimajorAxis ** 2
                                              - SemiminorAxis ** 2) / SemiminorAxis ** 2
            Abar = 1 + uSquared * (4096 + uSquared * (-768 + uSquared
                                   * (320 - 175 * uSquared))) / 16384
            Bbar = uSquared * (256 + uSquared * (-128 + uSquared * (74
                               - 47 * uSquared))) / 1024
            DeltaSigma = Bbar * sinsigma * (cosTwoSigmaMid + 0.25
                                            * Bbar * (cossigma * (-1 + 2 * cosTwoSigmaMid ** 2)
                                                      - Bbar * cosTwoSigmaMid *
                                                      (-3 + 4 *
                                                       sinsigma ** 2)
                                                      * (-3 + 4 * cosTwoSigmaMid ** 2) / 6))
            s = SemiminorAxis * Abar * (sigma - DeltaSigma)
        except Exception:
            s = 0
        try:
            Convergence = np.abs(s - oldS)
        except Exception:
            pass
        oldS = s
    return s


def SphericalDistance(StartLat, StartLon, EndLat, EndLon):
    L = np.radians(EndLon - StartLon)
    deltaLat = np.radians(EndLat - StartLat)
    StartLat = np.radians(StartLat)
    EndLat = np.radians(EndLat)
    Lat = 0.5 * (StartLat + EndLat)
    # a better approximation of the average latitude would be this if deltaLat>L, and max(StartLat,EndLat) otherwise
    radius = np.sqrt(((SemimajorAxis ** 2 * np.cos(Lat)) ** 2
                     + (SemiminorAxis ** 2 * np.sin(Lat)) ** 2)
                     / ((SemimajorAxis * np.cos(Lat)) ** 2
                     + (SemiminorAxis * np.sin(Lat)) ** 2))
    # midpoint Lat should be np.atan2(np.sin(StartLat) + np.sin(EndLat), np.sqrt((cos(StartLat) + cos(EndLat)*cos(L))**2 + (cos(EndLat)*sin(L))**2)); but this is not the average latitude
    if np.all(np.absolute(EndLon - StartLon) < 150):
        optimized = 2 * radius \
            * np.arcsin(np.sqrt(np.square(np.sin(deltaLat / 2))
                        + np.cos(StartLat) * np.cos(EndLat)
                        * np.square(np.sin(L / 2))))
    else:
        optimized = np.arctan(np.sqrt(np.square(np.cos(EndLat)
                              * sin(L))
            + np.square(np.cos(StartLat)
                        * np.sin(EndLat) - np.sin(StartLat)
                        * np.cos(EndLat) * np.cos(L)))
            / (np.sin(StartLat) * np.sin(EndLat)
               + np.cos(StartLat) * np.cos(EndLat)
               * np.cos(L)))
    return optimized


im = Image.open('USA_population_density.png')# thank_you_Chika
im = im.convert('L')
imar = np.asarray(im)

Scale = 1
num_cells = len(SeedLats)
northmost = 5000 / Scale
eastmost = -6600 / Scale
southmost = 2400 / Scale
westmost = -12500 / Scale
width = int(eastmost - westmost)
height = int(northmost - southmost)
image = Image.new('RGB', (width, height), (220, 220, 220))
putpixel = image.putpixel
draw = ImageDraw.Draw(image)
(imgx, imgy) = image.size
maparray = np.zeros((height, width))  # ,3))

shortest = np.empty([num_cells])
maxshortest = np.pi * SemimajorAxis
rmaxshortest = maxshortest
for (start, end) in itertools.product(range(num_cells),
                                      range(num_cells)):
    if end != start:
        realshortest = SphericalDistance(SeedLats[start],
                                         SeedLons[start], SeedLats[end], SeedLons[end])
        euclideanshortest = math.hypot(SeedLats[start] - SeedLats[end],
                                       SeedLons[start] - SeedLons[end])
        if euclideanshortest < maxshortest:
            maxshortest = euclideanshortest
        if realshortest < rmaxshortest:
            rmaxshortest = realshortest
            ermaxshortest = math.hypot(SeedLats[start] - SeedLats[end],
                                       SeedLons[start] - SeedLons[end])
        try:
            shortest[start] = min(ermaxshortest, maxshortest) / Scale \
                / np.e
        except NameError:
            shortest[start] = maxshortest / Scale / np.e
        if end == num_cells - 1:
            maxshortest = np.pi * SemimajorAxis
            rmaxshortest = maxshortest
rootshortest = np.sqrt(shortest)
nh = 100 * (SeedLats - Scale * southmost / 100) / height
nv = 0.4 + 40 * (SeedLons - Scale * westmost / 100) / width
nr = np.empty(num_cells)
ng = np.empty(num_cells)
nb = np.empty(num_cells)
for n in range(num_cells):
    nrgb = colorsys.hsv_to_rgb(nh[n], 1, nv[n])
    nr[n] = nrgb[0]
    ng[n] = nrgb[1]
    nb[n] = nrgb[2]
nr = np.ceil(nr * 256).astype(int)
ng = np.ceil(ng * 256).astype(int)
nb = np.ceil(nb * 256).astype(int)

# grid 256 wide

for (x, y) in itertools.product(range(math.ceil(imgx / 256)),
                                range(imgy)):
    x = 256 * x
    if im.getpixel((x, y)) != 255:
        if np.nanmin(np.absolute(np.hypot(Scale * (northmost - y) / 100
                     - SeedLats, Scale * (westmost + x) / 100
                     - SeedLons))) < np.nanmax(shortest):
            for i in range(num_cells):
                if np.absolute(np.hypot(Scale * (northmost - y) / 100
                               - SeedLats[i], Scale * (westmost + x)
                               / 100 - SeedLons[i])) < shortest[i]:
                    j = i
                    putpixel((x, y), (nr[j], ng[j], nb[j]))
                    break
                elif i == num_cells - 1:
                    j = np.argmin(SphericalDistance(Scale * (northmost
                                  - y) / 100, Scale * (westmost + x)
                                  / 100, SeedLats, SeedLons))
                    putpixel((x, y), (nr[j], ng[j], nb[j]))
        else:
            j = np.argmin(SphericalDistance(Scale * (northmost - y)
                          / 100, Scale * (westmost + x) / 100,
                          SeedLats, SeedLons))
            putpixel((x, y), (nr[j], ng[j], nb[j]))
for (x, y) in itertools.product(range(imgx), range(math.ceil(imgy
                                / 256))):
    y = 256 * y
    if im.getpixel((x, y)) != 255 and image.getpixel((x, y)) == (220,
                                                                 220, 220):
        if np.nanmin(np.absolute(np.hypot(Scale * (northmost - y) / 100
                     - SeedLats, Scale * (westmost + x) / 100
                     - SeedLons))) < np.nanmax(shortest):
            for i in range(num_cells):
                if np.absolute(np.hypot(Scale * (northmost - y) / 100
                               - SeedLats[i], Scale * (westmost + x)
                               / 100 - SeedLons[i])) < shortest[i]:
                    j = i
                    putpixel((x, y), (nr[j], ng[j], nb[j]))
                    break
                elif i == num_cells - 1:
                    j = np.argmin(SphericalDistance(Scale * (northmost
                                  - y) / 100, Scale * (westmost + x)
                                  / 100, SeedLats, SeedLons))
                    putpixel((x, y), (nr[j], ng[j], nb[j]))
        else:
            j = np.argmin(SphericalDistance(Scale * (northmost - y)
                          / 100, Scale * (westmost + x) / 100,
                          SeedLats, SeedLons))
            putpixel((x, y), (nr[j], ng[j], nb[j]))

# grid 128

for (x, y) in itertools.product(range(math.ceil(imgx / 128) - 2),
                                range(math.ceil(imgy / 128) - 2)):
    x = 128 * x + 128
    y = 128 * y + 128
    if im.getpixel((x, y)) != 255 and image.getpixel((x, y)) == (220,
                                                                 220, 220):
        sides = 1
        topleft = image.getpixel((x - 128, y - 128))
        for n in range(-128, 128):  # top and bottom of square
            if image.getpixel((x - n, y - 128)) == topleft \
                    and image.getpixel((x - n, y + 128)) == topleft:
                pass
            else:
                sides = 0
                break
        if sides == 1:
            for n in range(-126, 126):  # sides
                if image.getpixel((x - 128, y - n)) == topleft \
                        and image.getpixel((x + 128, y - n)) == topleft:
                    pass
                else:
                    sides = 0
                    break
        if sides == 1:
            draw.rectangle([(x - 127, y + 127), (x + 128, y - 128)],
                           fill=topleft, outline=topleft)
for (x, y) in itertools.product(range(math.ceil(imgx / 128)),
                                range(imgy)):
    x = 128 * x
    if im.getpixel((x, y)) != 255:
        if np.nanmin(np.absolute(np.hypot(Scale * (northmost - y) / 100
                     - SeedLats, Scale * (westmost + x) / 100
                     - SeedLons))) < np.nanmax(shortest):
            for i in range(num_cells):
                if np.absolute(np.hypot(Scale * (northmost - y) / 100
                               - SeedLats[i], Scale * (westmost + x)
                               / 100 - SeedLons[i])) < shortest[i]:
                    j = i
                    putpixel((x, y), (nr[j], ng[j], nb[j]))
                    break
                elif i == num_cells - 1:
                    j = np.argmin(SphericalDistance(Scale * (northmost
                                  - y) / 100, Scale * (westmost + x)
                                  / 100, SeedLats, SeedLons))
                    putpixel((x, y), (nr[j], ng[j], nb[j]))
        else:
            j = np.argmin(SphericalDistance(Scale * (northmost - y)
                          / 100, Scale * (westmost + x) / 100,
                          SeedLats, SeedLons))
            putpixel((x, y), (nr[j], ng[j], nb[j]))
for (x, y) in itertools.product(range(imgx), range(math.ceil(imgy
                                / 128))):
    y = 128 * y
    if im.getpixel((x, y)) != 255 and image.getpixel((x, y)) == (220,
                                                                 220, 220):
        if np.nanmin(np.absolute(np.hypot(Scale * (northmost - y) / 100
                     - SeedLats, Scale * (westmost + x) / 100
                     - SeedLons))) < np.nanmax(shortest):
            for i in range(num_cells):
                if np.absolute(np.hypot(Scale * (northmost - y) / 100
                               - SeedLats[i], Scale * (westmost + x)
                               / 100 - SeedLons[i])) < shortest[i]:
                    j = i
                    putpixel((x, y), (nr[j], ng[j], nb[j]))
                    break
                elif i == num_cells - 1:
                    j = np.argmin(SphericalDistance(Scale * (northmost
                                  - y) / 100, Scale * (westmost + x)
                                  / 100, SeedLats, SeedLons))
                    putpixel((x, y), (nr[j], ng[j], nb[j]))
        else:
            j = np.argmin(SphericalDistance(Scale * (northmost - y)
                          / 100, Scale * (westmost + x) / 100,
                          SeedLats, SeedLons))
            putpixel((x, y), (nr[j], ng[j], nb[j]))

# end 128
# start 64

for (x, y) in itertools.product(range(math.ceil(imgx / 64) - 2),
                                range(math.ceil(imgy / 64) - 2)):
    x = 64 * x + 64
    y = 64 * y + 64
    if im.getpixel((x, y)) != 255 and image.getpixel((x, y)) == (220,
                                                                 220, 220):
        sides = 1
        topleft = image.getpixel((x - 64, y - 64))
        for n in range(-64, 64):  # top and bottom of square
            if image.getpixel((x - n, y - 64)) == topleft \
                    and image.getpixel((x - n, y + 64)) == topleft:
                pass
            else:
                sides = 0
                break
        if sides == 1:
            for n in range(-62, 62):  # sides
                if image.getpixel((x - 64, y - n)) == topleft \
                        and image.getpixel((x + 64, y - n)) == topleft:
                    pass
                else:
                    sides = 0
                    break
        if sides == 1:
            draw.rectangle([(x - 63, y + 63), (x + 64, y - 64)],
                           fill=topleft, outline=topleft)
for (x, y) in itertools.product(range(math.ceil(imgx / 64)),
                                range(imgy)):
    x = 64 * x
    if im.getpixel((x, y)) != 255:
        if np.nanmin(np.absolute(np.hypot(Scale * (northmost - y) / 100
                     - SeedLats, Scale * (westmost + x) / 100
                     - SeedLons))) < np.nanmax(shortest):
            for i in range(num_cells):
                if np.absolute(np.hypot(Scale * (northmost - y) / 100
                               - SeedLats[i], Scale * (westmost + x)
                               / 100 - SeedLons[i])) < shortest[i]:
                    j = i
                    putpixel((x, y), (nr[j], ng[j], nb[j]))
                    break
                elif i == num_cells - 1:
                    j = np.argmin(SphericalDistance(Scale * (northmost
                                  - y) / 100, Scale * (westmost + x)
                                  / 100, SeedLats, SeedLons))
                    putpixel((x, y), (nr[j], ng[j], nb[j]))
        else:
            j = np.argmin(SphericalDistance(Scale * (northmost - y)
                          / 100, Scale * (westmost + x) / 100,
                          SeedLats, SeedLons))
            putpixel((x, y), (nr[j], ng[j], nb[j]))
for (x, y) in itertools.product(range(imgx), range(math.ceil(imgy
                                / 64))):
    y = 64 * y
    if im.getpixel((x, y)) != 255 and image.getpixel((x, y)) == (220,
                                                                 220, 220):
        if np.nanmin(np.absolute(np.hypot(Scale * (northmost - y) / 100
                     - SeedLats, Scale * (westmost + x) / 100
                     - SeedLons))) < np.nanmax(shortest):
            for i in range(num_cells):
                if np.absolute(np.hypot(Scale * (northmost - y) / 100
                               - SeedLats[i], Scale * (westmost + x)
                               / 100 - SeedLons[i])) < shortest[i]:
                    j = i
                    putpixel((x, y), (nr[j], ng[j], nb[j]))
                    break
                elif i == num_cells - 1:
                    j = np.argmin(SphericalDistance(Scale * (northmost
                                  - y) / 100, Scale * (westmost + x)
                                  / 100, SeedLats, SeedLons))
                    putpixel((x, y), (nr[j], ng[j], nb[j]))
        else:
            j = np.argmin(SphericalDistance(Scale * (northmost - y)
                          / 100, Scale * (westmost + x) / 100,
                          SeedLats, SeedLons))
            putpixel((x, y), (nr[j], ng[j], nb[j]))

# end 64
# start grid 32

for (x, y) in itertools.product(range(math.ceil(imgx / 32) - 2),
                                range(math.ceil(imgy / 32) - 2)):
    x = 32 * x + 32
    y = 32 * y + 32
    if im.getpixel((x, y)) != 255 and image.getpixel((x, y)) == (220,
                                                                 220, 220):
        sides = 1
        topleft = image.getpixel((x - 32, y - 32))
        for n in range(-32, 32):  # top and bottom of square
            if image.getpixel((x - n, y - 32)) == topleft \
                    and image.getpixel((x - n, y + 32)) == topleft:
                pass
            else:
                sides = 0
                break
        if sides == 1:
            for n in range(-30, 30):  # sides
                if image.getpixel((x - 32, y - n)) == topleft \
                        and image.getpixel((x + 32, y - n)) == topleft:
                    pass
                else:
                    sides = 0
                    break
        if sides == 1:
            draw.rectangle([(x - 31, y + 31), (x + 32, y - 32)],
                           fill=topleft, outline=topleft)
for (x, y) in itertools.product(range(math.ceil(imgx / 32)),
                                range(imgy)):
    x = 32 * x
    if im.getpixel((x, y)) != 255:
        if np.nanmin(np.absolute(np.hypot(Scale * (northmost - y) / 100
                     - SeedLats, Scale * (westmost + x) / 100
                     - SeedLons))) < np.nanmax(shortest):
            for i in range(num_cells):
                if np.absolute(np.hypot(Scale * (northmost - y) / 100
                               - SeedLats[i], Scale * (westmost + x)
                               / 100 - SeedLons[i])) < shortest[i]:
                    j = i
                    putpixel((x, y), (nr[j], ng[j], nb[j]))
                    break
                elif i == num_cells - 1:
                    j = np.argmin(SphericalDistance(Scale * (northmost
                                  - y) / 100, Scale * (westmost + x)
                                  / 100, SeedLats, SeedLons))
                    putpixel((x, y), (nr[j], ng[j], nb[j]))
        else:
            j = np.argmin(SphericalDistance(Scale * (northmost - y)
                          / 100, Scale * (westmost + x) / 100,
                          SeedLats, SeedLons))
            putpixel((x, y), (nr[j], ng[j], nb[j]))
for (x, y) in itertools.product(range(imgx), range(math.ceil(imgy
                                / 32))):
    y = 32 * y
    if im.getpixel((x, y)) != 255 and image.getpixel((x, y)) == (220,
                                                                 220, 220):
        if np.nanmin(np.absolute(np.hypot(Scale * (northmost - y) / 100
                     - SeedLats, Scale * (westmost + x) / 100
                     - SeedLons))) < np.nanmax(shortest):
            for i in range(num_cells):
                if np.absolute(np.hypot(Scale * (northmost - y) / 100
                               - SeedLats[i], Scale * (westmost + x)
                               / 100 - SeedLons[i])) < shortest[i]:
                    j = i
                    putpixel((x, y), (nr[j], ng[j], nb[j]))
                    break
                elif i == num_cells - 1:
                    j = np.argmin(SphericalDistance(Scale * (northmost
                                  - y) / 100, Scale * (westmost + x)
                                  / 100, SeedLats, SeedLons))
                    putpixel((x, y), (nr[j], ng[j], nb[j]))
        else:
            j = np.argmin(SphericalDistance(Scale * (northmost - y)
                          / 100, Scale * (westmost + x) / 100,
                          SeedLats, SeedLons))
            putpixel((x, y), (nr[j], ng[j], nb[j]))

# end 32
# grid 16

for (x, y) in itertools.product(range(math.ceil(imgx / 16) - 2),
                                range(math.ceil(imgy / 16) - 2)):
    x = 16 * x + 16
    y = 16 * y + 16
    if im.getpixel((x, y)) != 255 and image.getpixel((x, y)) == (220,
                                                                 220, 220):
        sides = 1
        topleft = image.getpixel((x - 16, y - 16))
        for n in range(-16, 16):  # top and bottom of square
            if image.getpixel((x - n, y - 16)) == topleft \
                    and image.getpixel((x - n, y + 16)) == topleft:
                pass
            else:
                sides = 0
                break
        if sides == 1:
            for n in range(-14, 14):  # sides
                if image.getpixel((x - 16, y - n)) == topleft \
                        and image.getpixel((x + 16, y - n)) == topleft:
                    pass
                else:
                    sides = 0
                    break
        if sides == 1:
            draw.rectangle([(x - 15, y + 15), (x + 16, y - 16)],
                           fill=topleft, outline=topleft)

for (x, y) in itertools.product(range(imgx), range(imgy)):
    if im.getpixel((x, y)) != 255:
        if np.nanmin(np.absolute(np.hypot(Scale * (northmost - y) / 100
                     - SeedLats, Scale * (westmost + x) / 100
                     - SeedLons))) < np.nanmax(shortest):
            for i in range(num_cells):
                if np.absolute(np.hypot(Scale * (northmost - y) / 100
                               - SeedLats[i], Scale * (westmost + x)
                               / 100 - SeedLons[i])) < shortest[i]:
                    j = i
                    maparray[y, x] = j
                    putpixel((x, y), (nr[j], ng[j], nb[j]))
                    break
                elif i == num_cells - 1:
                    j = np.argmin(SphericalDistance(Scale * (northmost
                                  - y) / 100, Scale * (westmost + x)
                                  / 100, SeedLats, SeedLons))
                    maparray[y, x] = j
                    putpixel((x, y), (nr[j], ng[j], nb[j]))
        else:
            j = np.argmin(SphericalDistance(Scale * (northmost - y)
                          / 100, Scale * (westmost + x) / 100,
                          SeedLats, SeedLons))
            maparray[y, x] = 1  # j+1
            putpixel((x, y), (nr[j], ng[j], nb[j]))

edge = image.copy()
edge = ImageOps.expand(edge, border=2, fill=(220, 220, 220))
edge = edge.filter(ImageFilter.Kernel((3, 3), (-1, -1, -1,
                                               -1, 8, -1,
                                               -1, -1, -1),
                                      1, 0)).convert('L').point(lambda x: (255 if x > 0 else 0),
                                                                mode='1').convert('L'
                                                                                  ).filter(ImageFilter.BoxBlur(2)).point(lambda x: (255 if x
                                                                                                                                    > 0 else 0), mode='1').crop((2, 2, width + 2,
                                                                                                                                                                 height + 2))

for (x, y) in itertools.product(range(imgx), range(imgy)):
    if edge.getpixel((x, y)) == 255:
        if np.nanmin(np.absolute(np.hypot(Scale * (northmost - y) / 100
                     - SeedLats, Scale * (westmost + x) / 100
                     - SeedLons))) < np.nanmax(shortest):
            for i in range(num_cells):
                if np.absolute(np.hypot(Scale * (northmost - y) / 100
                               - SeedLats[i], Scale * (westmost + x)
                               / 100 - SeedLons[i])) < shortest[i]:
                    j = i
                    maparray[y, x] = j
                    putpixel((x, y), (nr[j], ng[j], nb[j]))
                    break
                elif i == num_cells - 1:
                    j = np.argmin(RealDistance(Scale * (northmost
                                  - y) / 100, Scale * (westmost + x)
                                  / 100, SeedLats, SeedLons))
                    maparray[y, x] = j
                    putpixel((x, y), (nr[j], ng[j], nb[j]))
        else:
            j = np.argmin(RealDistance(Scale * (northmost - y)
                          / 100, Scale * (westmost + x) / 100,
                          SeedLats, SeedLons))
            maparray[y, x] = 1  # j+1
            putpixel((x, y), (nr[j], ng[j], nb[j]))

image.save('VoronoiDiagram.png', 'PNG')
period = time.time() - Ek
image.show()

print(period)
